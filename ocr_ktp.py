#!/usr/bin/python

import re
import cv2 as oc
import pytesseract as ract
import json as js

ktp = "sample/ktp2.png"

def ocrKtp(image):
    img = oc.imread(image)
    img = oc.cvtColor(img, oc.COLOR_BGR2GRAY)
    th, threshed = oc.threshold(img, 127, 255, oc.THRESH_TRUNC)
    text = ract.image_to_string(threshed, lang="ind")
    return text

dataKtp = ocrKtp(ktp)
dataKtpList = []
for data in dataKtp.split("\n"):
    if data != "" and data != " ":
        dataKtpList.append(data)

def nikKtp():
    nik = partKtp("NIK")
    nik = nik.replace("?", "7")
    nik = nik.replace("L", "1")
    nik = nik.replace("l", "1")
    nik = nik.replace("i", "1")
    nik = nik.replace("o", "0")
    nik = nik.replace("D", "0")
    nik = nik.replace("S", "5")
    nik = nik.replace("g", "9")
    nik = nik.replace(" ", "")
    nik = nik.replace(".", "")
    return nik

def prov():
    provinsi = filter(lambda kata:"PROVINSI" in kata, dataKtpList)
    provinsi = list(provinsi)
    if len(provinsi) < 1:
        provinsi = ""
    else:
        provinsi = provinsi[0]
    return provinsi

def kabKota():
    provinsi = prov()
    indexProv = dataKtpList.index(provinsi)
    kab = dataKtpList[indexProv+1]
    return kab

def partKtp(partName):
    part = filter(lambda kata:partName in kata, dataKtpList)
    part = list(part)
    if len(part) >= 1:
        for p in part:
            p = p.split(":")
            if len(p) <= 1:
                p = ""
            elif p[1] == "":
                p = ""
            else:
                p = p[1]
                p = p.split()
                if p[0] == "":
                    p = " ".joi(p[1:])
                else:
                    p = " ".join(p)
    else:
        p = ""
    return p

def partAlamat():
    alamat = filter(lambda kata:"Alamat" in kata, dataKtpList)
    alamat = list(alamat)
    rtRw = filter(lambda kata:"RT/RW" in kata, dataKtpList)
    rtRw = list(rtRw)
    if len(rtRw) < 1:
        rtRw = filter(lambda kata:"RTRW" in kata, dataKtpList)
        rtRw = list(rtRw)
    kataHapus = ["Alamat ", "Alamat", ": ", ":"]
    if len(alamat) >= 1:
        if len(rtRw) >= 1:
            indexAlamat = dataKtpList.index(alamat[0])
            if dataKtpList[indexAlamat+1] == rtRw[0]:
                alamat = alamat[0]
                for kata in kataHapus:
                    alamat = alamat.replace(kata, "")
            else:
                alamat = alamat[0]
                alamatL = dataKtpList[indexAlamat+1]
                alamat = f"{alamat} {alamatL}"
                for kata in kataHapus:
                    alamat = alamat.replace(kata, "")
        else:
            alamat = alamat[0]
            for kata in kataHapus:
                alamat = alamat.replace(kata, "")
    else:
        alamat = ""
    return alamat

def rtrw():
    rtRw = partKtp("RT/RW")
    if rtRw == "":
        rtRw = partKtp("RTRW")
    elif rtRw != "":
        rtRwL = []
        for x in rtRw:
            rtRwL.append(x)
        if "/" not in rtRwL:
            rtRwL.insert(3, "/")
            rtRw = "".join(rtRwL)
    else:
        rtRw = ""
    return rtRw

def pekerjaan():
    pekerjaan = partKtp("Pekerjaan")
    for x in range(0, 9):
        x = str(x)
        pekerjaan = pekerjaan.replace(x, "")
    pekerjaan = pekerjaan.replace("-", "")
    return pekerjaan

finalResult = {
        "Provinsi" : prov(),
        "Kab./ Kota":  kabKota(),
        "Nik": nikKtp(),
        "Nama": partKtp("Nama"),
        "Tempat/Tgl Lahir": partKtp("Lahir"),
#        "Jenis Kelamin": jk,
        "Alamat": partAlamat(),
        "RT/RW": rtrw(),
        "Kel/Desa": partKtp("Kel/Desa"),
        "Kecamatan": partKtp("Kecamatan"),
        "Agama": partKtp("Agama"),
        "Status Perkawinan": partKtp("Status Perkawinan"),
        "Pekerjaan": pekerjaan(),
        "Kewarganegaraan": partKtp("Kewarganegaraan")
        }

jsonResult = js.dumps(finalResult, indent=4)
print(jsonResult)
